if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let g:startify_custom_header = [
        \ '   _  __     _         __  ___         __     ___ ',
        \ '  / |/ /  __(_)_ _    /  |/  /__ _____/ /    |_  |',
        \ ' /    / |/ / /  ` \  / /|_/ / _ `/ __/ _ \  / __/ ',
        \ '/_/|_/|___/_/_/_/_/ /_/  /_/\_,_/\__/_//_/ /____/ ',
        \]

call plug#begin('~/.vim/plugged')
    Plug 'junegunn/vim-easy-align/'

    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf.vim',
    Plug 'pbogut/fzf-mru.vim',
    Plug 'junegunn/gv.vim'
    Plug 'jreybert/vimagit'

    Plug 'janko/vim-test'

    Plug 'NLKNguyen/papercolor-theme'
    Plug 'vim-airline/vim-airline'
    " Plug 'vim-airline/vim-airline-themes'
    Plug 'dracula/vim', { 'as': 'dracula' }
    Plug 'lifepillar/vim-solarized8'

    Plug 'scrooloose/nerdtree'
    Plug 'wesQ3/vim-windowswap'
    Plug 'majutsushi/tagbar'
    Plug 'tpope/vim-obsession'
    Plug 'tpope/vim-dispatch'
    Plug 'tmux-plugins/vim-tmux-focus-events'
    Plug 'moll/vim-bbye'
    Plug 'mhinz/vim-startify'
    Plug 'AndrewRadev/tagalong.vim'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/limelight.vim'
    Plug 'amix/vim-zenroom2'

    if version >= 800
        Plug 'neoclide/coc.nvim', {'branch': 'release'}
    endif

    Plug 'tpope/vim-surround'
    Plug 'pelodelfuego/vim-swoop'
    Plug 'dense-analysis/ale'
    Plug 'tpope/vim-commentary'

    " Plug 'kovisoft/slimv'
    Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

    Plug 'posva/vim-vue'
    Plug 'ap/vim-css-color'
    Plug 'pangloss/vim-javascript'
    Plug 'othree/html5.vim'
    Plug 'matthew-brett/vim-rst-sections'
    Plug 'cespare/vim-toml'
    Plug 'mustache/vim-mustache-handlebars'
    Plug 'Glench/Vim-Jinja2-Syntax'
    Plug 'evanleck/vim-svelte'
    Plug 'leafgarland/typescript-vim'
    Plug 'HerringtonDarkholme/yats.vim'
    Plug 'elixir-editors/vim-elixir'
    Plug 'elzr/vim-json'
    Plug 'rust-lang/rust.vim'
    Plug 'delphinus/vim-firestore'
    Plug 'sebdah/vim-delve'
    Plug 'guns/vim-clojure-static'

    Plug 'regedarek/ZoomWin'
call plug#end()

let g:coc_disable_startup_warning = 1

" set guifont="Inconsolata-dz Medium 12"
if has("gui_running")
  if has("gui_gtk3")
    :set guifont="Fira Code Medium 11,Monospace 11"
  elseif has('macunix')
  	set guifont="FiraCode-Retina:h14,Fira Code Medium,Inconsolata-dz:h12"
  elseif has("x11")
    " Also for GTK 1
    :set guifont=*-lucidatypewriter-medium-r-normal-*-*-180-*-*-m-*-*
  elseif has("gui_win32")
    :set guifont=Fira\ Code\ Medium\ 11;Luxi_Mono:h12:cANSI
  endif
endif

" set guifont="Fira Code Medium"
" if has('macunix')
" 	set guifont="Fira Code Medium 11,Inconsolata-dz:h12"
" else
" 	set guifont="Inconsolata-dz Medium 12"
" endif
let mapleader       = ' '
let maplocalleader  = ','

" Edit another file in the same directory as the current file. Uses expression
" to extract path from current file's path (thanks to Douglas Potts)
if has("unix")
    map ,e :e <C-R>=expand("%:p:h") . "/"<CR>
else
    map ,e :e <C-R>=expand("%:p:h") . "\\"<CR>
endif

map <leader>ww 0v$gq
map =j :%!jq .<CR>

set backupdir=/tmp//,.
set directory=/tmp//,.

" Enable visible line numbers
set number

" When we return to Vim, or re-enter a buffer, check if it's been modified.
set autoread
au FocusGained,BufEnter * :checktime

" Make the current word upper-case; useful for constants.
nnoremap <leader>wu  viwU
nnoremap <leader>wl  viwu
nnoremap <C-U>  viwU
nnoremap <C-L>  viwu

nnoremap <leader>td :colorscheme dracula<CR>
nnoremap <leader>ts :colorscheme solarized8<CR>
nnoremap <leader>tp :colorscheme PaperColor<CR>
nnoremap <leader>bd :set background=dark<CR>
nnoremap <leader>bl :set background=light<CR>

nnoremap <c-s-up> dd2kp
nnoremap <c-s-down> ddp

nnoremap ,m :Make<CR>
nnoremap <leader>q :Bdelete<CR>

" Repeat the last macro
nnoremap Q @@

" Close all folds
nnoremap c- zM

nnoremap <leader>cw y/ <CR>
nnoremap <leader>wp 0v$gq

inoremap jk     <esc>
" inoremap <esc>  <nop>
" Use 'p' to mean 'inside parantheses' in commands.
onoremap p i(

let g:magit_discard_untracked_do_delete=1
nnoremap ,gr :!git review<CR>
nnoremap ,gp :!git push<CR>
nnoremap ,gl :!git pull<CR>
nnoremap ,gb :!git branch -l<CR>
nnoremap ,gc :!git checkout -b 
nnoremap ,yt :!yarn test<CR>

nnoremap ,m  :Make<CR>

" colorscheme PaperColor
set background=dark
" colorscheme dracula
colorscheme solarized8

set ignorecase smartcase
set incsearch

set complete=.,b,u,]

set wildignore+=deps/*,node_modules/*,*.sw?

" Turn off the beeping ('cause it's annoying)
set noerrorbells
set visualbell
set t_vb=

" Auto-indent code
set autoindent

" do not redraw the screen during macro execution
set lazyredraw

" Open new panes to right and bottom.
set splitbelow
set splitright

map <C-T> :pop<CR>

" tabbing and indent options
function! TabSize (size)
  if a:size==8
    set noexpandtab
  else
    set expandtab
  endif
  exe 'set shiftwidth=' . a:size
  exe 'set tabstop=' . a:size
  return "Tab size = " . a:size
endfunction
call TabSize(2)
map ,t2   :echo TabSize(2)<CR>
map ,t3   :echo TabSize(3)<CR>
map ,t4   :echo TabSize(4)<CR>
map ,t8   :echo TabSize(8)<CR>

" Explicitly set certain filetypes.
augroup filetype
  au!
  au BufRead,BufNewFile *.S	set filetype=c
  au bufRead,BufNewFile *.cc set filetype=cpp
  au bufRead,BufNewFile *.cpp set filetype=cpp
  au bufRead,BufNewFile *.inc set filetype=make
  au BufRead,BufNewFile *.eex set filetype=html
  au BufRead,BufNewFile *.bss set filetype=vb
  au BufRead,BufNewFile *.asm set filetype=nasm
augroup END

augroup java
  au!
  inoremap sop System.out.println(
augroup END

augroup cppprog
	au!
	au BufRead,BufNewFile *.h        set cindent
	au BufRead,BufNewFile *.cc       set cindent
	au BufRead,BufNewFile *.cpp      set cindent
augroup END

augroup cprog
	au!
	au BufRead,BufNewFile *.[ch]     set cindent
augroup END

augroup xml
	autocmd!
	autocmd FileType xml let g:xml_syntax_folding=1
	autocmd FileType xml setlocal foldmethod=syntax
	autocmd FileType xml :syntax on
	autocmd FileType xml :%foldopen!
augroup END

" Apply the muttrc colouring to mutt setup files:
au BufNewFile,BufRead .mutt.*        set ft=muttrc

augroup elixir
  autocmd!
  autocmd BufRead,BufNewFile *.ex,*.exs inoremap ii  IO.inspect(
  autocmd BufRead,BufNewFile *.ex,*.exs inoremap ip  IO.puts(
augroup END

augroup filetype vue
  autocmd!
  autocmd BufRead,BufNewFile *.vue inoremap cll console.log()<left>
  iabbrev template  <template><cr><div><cr></div><cr></template
  iabbrev script    <script><cr>export default {<cr>name: ""<cr>};<cr></script
  iabbrev stylescope     <style scoped><cr></style
augroup END

augroup filetype javascript
  autocmd!
  autocmd BufRead,BufNewFile *.js inoremap cll console.log()<left>
  iabbrev <buffer> iff 	if()<left>
augroup END

augroup filetype typescript
  autocmd!
  autocmd BufRead,BufNewFile *.ts inoremap cll console.log()<left>
  iabbrev <buffer> iff 	if()<left>
  iabbrev hsig async function NAME(request: Request, h: ResponseToolkit) {<ESC>?NAME<CR><left>
augroup END

" augroup filetype make
"   autocmd!
"   " call TabSize(8)
"   set noexpandtab
" augroup END

" Automatic typo fixing
iab compomemt component
iab seahores  seahorse
iab KREN_EMERG KERN_EMERG

" Disable the command 'K' (keyword lookup) 
" map K <NUL>

" List current buffers
map <C-L> :buffers<CR>
" Reformat selected text; normal mode only so we don't accidentally wrap a
" paragraph by typing that string!
nmap <leader>wp 0v$gq

" Copy word.
nmap <leader>cw y/ <CR>

" Save
map <C-S> :w<CR>

" Needed because PuTTY doesn't seem to send the usual ctrl-up/down sequences
map <ESC>[A     <C-Up>
map <ESC>[B     <C-Down>
map <ESC>[C     <C-Right>
map <ESC>[D     <C-Left>

" And these are for Mobaxterm
map <ESC>[1;5A  <C-Up>
map <ESC>[1;5B  <C-Down>
map <ESC>[1;5C  <C-Right>
map <ESC>[1;5D  <C-Left>
map <ESC>[H     <Home>
map <ESC>[F     <End>

map <C-Up>      :wincmd k<CR>
map <C-Down>    :wincmd j<CR>
map <C-Left>    :wincmd h<CR>
map <C-Right>   :wincmd l<CR>

" Don't automatically resize windows to be the same.
set noequalalways
" Fix the width of the window
nnoremap <leader>wfw :set winfixwidth
" Fix the height of the window
nnoremap <leader>wfh :set winfixheight

" map <C-Up> :wincmd k<CR>
" map <C-Down> :wincmd j<CR>

" Replay keystrokes
map <C-CR> @a

" map <C-CR> ?"<CR><RIGHT>v/"<CR><LEFT>"*y:e "*p

" Set swapsync to 'nothing' - potentially more data loss (no forced write to disk
" on swap write), but makes writes faster. I'm willing to take the chance. :-)
" Not supported on Neovim.
if !has("nvim")
    set sws=
endif

" Set showmatch - this flashes the cursor briefly at the matching ( or {
" when ) or } is entered. (If user keeps typing, cursor just pops up very
" quickly, then comes back.)
set showmatch

" The command {number}CTRL-G show the current nuffer number, too.
" This is yet another feature that vi does not have.
" As I always want to see the buffer number I map it to CTRL-G.
" Pleae note that here we need to prevent a loop in the mapping by
" using the comamnd "noremap"!
noremap <C-G> 2<C-G>

" ===================================================================
" VIM - Editing and updating the vimrc:

" Re-source and edit vimrc, respectively
nnoremap  ,u :source $MYVIMRC<CR>
nnoremap  ,v :edit   $MYVIMRC<CR>
nnoremap ,i :PlugInstall<CR>


nn  ,s :mks! Session.vim<CR>

" For some reason, the control-up/down doesn't work through putty, so create
" alternates as well.
nn  ,k :wincmd h<CR>
nn  ,l :wincmd j<CR>
nn  ,p :wincmd k<CR>
nn  ,; :wincmd l<CR>

" ===================================================================

"      ;rcm = remove "control-m"s - for those mails sent from DOS:
nn   ,rcm :%s/<C-V><C-M>$//g<CR>

"      ,ksr = "kill space runs"
"             substitutes runs of two or more space to a single space:
nmap ,ksr :%s/  \+/ /g
vmap ,ksr  :s/  \+/ /g

"    ,Sel = "squeeze empty lines"
"    Convert blocks of empty lines (not even whitespace included)
"    into *one* empty line (within current visual):
map ,Sel :g/^$/,/./-j

"    ,Sbl = "squeeze blank lines"
"    Convert all blocks of blank lines (containing whitespace only)
"    into *one* empty line (within current visual):
map ,Sbl :g/^\s*$/,/\S/-j

" make backspace more like a 'normal' editor (help options / backspace)
set backspace=1

" make the mouse active when run in an XTerm (this may disable middle click
" pasting)
if has("mouse")
    set mouse=ac
endif

" automatically write files on :make
set autowrite

" automatically indent code etc
set autoindent

" show file position
set ruler

" Don't highlight search targets (irritating)
" set nohlsearch

" Return makes a new line at the cursor without entering insert mode
nmap <CR> i<CR><ESC>

" Cut text down to size (C-J does not pre-join the lines)
map <C-J> 74\|bi<CR><ESC>
map <C-S-J> J74\|bi<CR><ESC>

" Use fzf to find files
map <C-P> :Files<CR>
map <C-O> :Files <C-R>=expand("%:p:h")<CR>
" nnoremap <silent> <leader>m :FZFMru<CR>
nnoremap <leader>n :NERDTreeToggle<cr>

" Use F6 to swap buffers
map       <F6>      :b #<CR>
nnoremap  <leader>b :b #<CR>

map //        gc$

nnoremap <leader>ss :syntax sync fromstart<CR>

nnoremap <leader>t  :TagbarToggle<CR>


" date functions
nnoremap ,dn i<CR><CR><ESC>k!!date +'\%d/\%m/\%y'<CR>kJJ
nnoremap ,dw i<CR><CR><ESC>k!!date +'\%V'<CR>kJJ

" xterm title magic
if "$TERM" != "dumb"
  set title
endif

" set up cscope support (:help cscope)
if has("cscope")
  set cscopetag
  set cscopetagorder=0
  set nocsverb
  " compress the path display to only the final 2 elements
  if has("cscopepathcomp")
    set cscopepathcomp=2
  endif
  set csverb
  map ,scs0 :cs add cscope.out<CR>
  map ,kcs0 :cs kill 0<CR>
  map ,ccs0 :!cscope -b -q -R<CR>
  map ,fs :cs find s <C-R>=expand("<cword>")<CR><CR>
  map ,fg :cs find g <C-R>=expand("<cword>")<CR><CR>
  map ,fd :cs find d <C-R>=expand("<cword>")<CR><CR>
  map ,fc :cs find c <C-R>=expand("<cword>")<CR><CR>
  map ,fa :cs find t <C-R>=expand("<cword>")<CR><CR>
  map ,fi :cs find i <C-R>=expand("%:t")<CR><CR>
  map ,ss :cs find s 
  map ,sg :cs find g 
  map ,sd :cs find d 
  map ,sc :cs find c 
  map ,sa :cs find t 
endif

" Avoids printing anything by accident...
set printexpr=PrintFile(v:fname_in)
function! PrintFile(fname)
	"call system("ghostview " . a:fname)
	"call system("lpr " . a:fname)
	call delete(a:fname)
	return v:shell_error
endfunction

function! ToggleWindowHorizontalVerticalSplit()
  if !exists('t:splitType')
    let t:splitType = 'vertical'
  endif

  if t:splitType == 'vertical' " is vertical switch to horizontal
    windo wincmd K
    let t:splitType = 'horizontal'
  else " is horizontal switch to vertical
    windo wincmd H
    let t:splitType = 'vertical'
  endif
endfunction

nnoremap <silent> <leader>wst :call ToggleWindowHorizontalVerticalSplit()<cr>

" COC configuration
if version >= 800
    " if hidden is not set, TextEdit might fail.
    set hidden

    " Some servers have issues with backup files, see #649
    set nobackup
    set nowritebackup

    " Better display for messages
    set cmdheight=2

    " You will have bad experience for diagnostic messages when it's default 4000.
    set updatetime=300

    " don't give |ins-completion-menu| messages.
    set shortmess+=c

    " always show signcolumns
    set signcolumn=yes

    " Use tab for trigger completion with characters ahead and navigate.
    " Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
    inoremap <silent><expr> <TAB>
          \ pumvisible() ? "\<C-n>" :
          \ <SID>check_back_space() ? "\<TAB>" :
          \ coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    function! s:check_back_space() abort
      let col = col('.') - 1
      return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

    " Use <c-space> to trigger completion.
    inoremap <silent><expr> <c-space> coc#refresh()

    " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
    " Coc only does snippet and additional edit on confirm.
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
    " Or use `complete_info` if your vim support it, like:
    " inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

    " Use `[g` and `]g` to navigate diagnostics
    nmap <silent> [g <Plug>(coc-diagnostic-prev)
    nmap <silent> ]g <Plug>(coc-diagnostic-next)

    " Remap keys for gotos
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)

    " Use K to show documentation in preview window
    nnoremap <silent> K :call <SID>show_documentation()<CR>

    function! s:show_documentation()
      if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
      else
        call CocAction('doHover')
      endif
    endfunction

    " Highlight symbol under cursor on CursorHold
    autocmd CursorHold * silent call CocActionAsync('highlight')

    " Remap for rename current word
    nmap <leader>rn <Plug>(coc-rename)

    " Remap for format selected region
    " xmap <leader>f  <Plug>(coc-format-selected)
    nmap <leader>rf  <Plug>(coc-format-selected)

    augroup mygroup
      autocmd!
      " Setup formatexpr specified filetype(s).
      autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
      " Update signature help on jump placeholder
      autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
    augroup end

    " Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
    xmap <leader>a  <Plug>(coc-codeaction-selected)
    nmap <leader>a  <Plug>(coc-codeaction-selected)

    " Remap for do codeAction of current line
    nmap <leader>ac  <Plug>(coc-codeaction)
    " Fix autofix problem of current line
    nmap <leader>qf  <Plug>(coc-fix-current)

    " Create mappings for function text object, requires document symbols feature of languageserver.
    xmap if <Plug>(coc-funcobj-i)
    xmap af <Plug>(coc-funcobj-a)
    omap if <Plug>(coc-funcobj-i)
    omap af <Plug>(coc-funcobj-a)

    " Use <TAB> for select selections ranges, needs server support, like: coc-tsserver, coc-python
    nmap <silent> <TAB> <Plug>(coc-range-select)
    xmap <silent> <TAB> <Plug>(coc-range-select)

    " Use `:Format` to format current buffer
    command! -nargs=0 Format :call CocAction('format')

    " Use `:Fold` to fold current buffer
    command! -nargs=? Fold :call     CocAction('fold', <f-args>)

    " use `:OR` for organize import of current buffer
    command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

    " Add status line support, for integration with other plugin, checkout `:h coc-status`
    set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

    " Using CocList
    " Show all diagnostics
    nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
    " Manage extensions
    nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
    " Show commands
    nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
    " Find symbol of current document
    nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
    " Search workspace symbols
    nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
    " Do default action for next item.
    nnoremap <silent> <space>j  :<C-u>CocNext<CR>
    " Do default action for previous item.
    nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
    " Resume latest coc list
    nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
endif

" <Tab> and <S-Tab> indent and unindent code
nmap <Tab> :><CR>
vmap <Tab> :><CR>
nmap <S-Tab> :<<CR>
vmap <S-Tab> :<<CR>
nnoremap <leader>f :Ag <C-R>=expand("<cword>")<CR><CR>

" these "Ctrl mappings" work well when Caps Lock is mapped to Ctrl
nmap <silent> tN :TestNearest<CR>
nmap <silent> tF :TestFile<CR>
nmap <silent> tS :TestSuite<CR>
nmap <silent> tL :TestLast<CR>
nmap <silent> tG :TestVisit<CR>d

" Opening with the default program
if has('macunix')
    " Open in default program
    nnoremap gF :!open <cfile><CR>
    " Open Finder
    nnoremap gH :!open %:p:h<CR>
    " Open in Safari
    nnoremap gB :!open -a Safari %<CR>
elseif has('unix')
    " Open in default program
    nnoremap gF :!xdg-open <cfile><CR>
    " Open file manager
    nnoremap gH :!xdg-open %:p:h<CR>
    " Open in Browser
    nnoremap gB :!xdg-open %<CR>
else
    echom "Don't know how to open on this platform."
endif

match Todo /\s\+$/

" Markdown folding
let g:markdown_fold_style = 'nested'

syntax on
filetype on
filetype plugin on
filetype plugin indent on

nnoremap <C-S-Right> :cn<CR>
nnoremap <C-S-Left> :cp<CR>
nnoremap <leader>z  :Goyo<CR>
" nmap ,l <Plug>(Limelight)
" xmap ,l <Plug>(Limelight)
nmap ,l :Limelight!!<CR>

set textwidth=120
