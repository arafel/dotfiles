# Setup fzf
# ---------
if [ ! -d "$HOME/.asdf/installs/fzf" ] ; then
    echo "fzf isn't installed via asdf. Checking for $HOME/.fzf"
    if [ ! -d "$HOME/.fzf" ] ; then
        echo "fzf is not installed."
        exit 1
    else
        echo "Found it. Consider installing fzf via asdf:"
        echo "asdf plugin add fzf"
        echo "asdf install fzf latest"
        echo "asdf global fzf latest"
    fi

    if [[ ! "$PATH" == *${HOME}/.fzf/bin* ]]; then
        export PATH="${PATH:+${PATH}:}${HOME}/.fzf/bin"
    fi
    COMPLETION_SCRIPT="${HOME}/.fzf/shell/completion.bash"
    BINDING_SCRIPT="${HOME}/.fzf/shell/key-bindings.bash"

else
    FZF_VERSION=$(asdf current fzf | awk '{print $2}')
    COMPLETION_SCRIPT="${HOME}/.asdf/installs/fzf/${FZF_VERSION}/shell/completion.bash"
    BINDING_SCRIPT="${HOME}/.asdf/installs/fzf/${FZF_VERSION}/shell/key-bindings.bash"
fi

export FZF_DEFAULT_COMMAND='fd --type f --color=never'
export FZF_ALT_C_COMMAND='fd --type d . --color=never'

# Auto-completion
# ---------------
[[ $- == *i* ]] && source $COMPLETION_SCRIPT 2> /dev/null

# Key bindings
# ------------
source $BINDING_SCRIPT

unset COMPLETION_SCRIPT
unset BINDING_SCRIPT
