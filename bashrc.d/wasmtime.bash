if [ -d "$HOME/.wasmtime" ] ; then
  export WASMTIME_HOME="$HOME/.wasmtime"
  addpath "$WASMTIME_HOME/bin"
fi
