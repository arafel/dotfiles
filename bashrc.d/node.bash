# Set up NPM
NPM_PACKAGES="$HOME/.npm-packages"
NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
MANPATH="$MANPATH:$NPM_PACKAGES/share/man"
addpath $NPM_PACKAGES/bin
addpath "$HOME/.yarn/bin"
addpath "$HOME/.config/yarn/global/node_modules/.bin"
