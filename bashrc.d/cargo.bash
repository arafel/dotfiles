# tabtab source for packages
# uninstall by removing these lines
[ -f ~/.config/tabtab/__tabtab.bash ] && . ~/.config/tabtab/__tabtab.bash || true
[ -f "$HOME/.cargo/env" ] && source "$HOME/.cargo/env" || true
[ -d "$HOME/.cargo/bin" ] && addpath "$HOME/.cargo/bin" || true
