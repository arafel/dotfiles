prompt() {
    if [ "$STY" != "" ] ; then
        SESSION_NAME=$(echo $STY | sed -e 's/^.*\.//')
        SESSION="[$SESSION_NAME] "
        . $HOME/data/screen/"$SESSION_NAME".display
    else
        SESSION=""
    fi
    export PS1="$SESSION\u@\h:\w$ "
}

export PROMPT_DIRTRIM=5
export PROMPT_COMMAND=prompt
