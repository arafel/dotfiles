if [ -x /usr/bin/dircolors ]; then
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

# some more ls aliases
alias ll='ls -l'
alias la="ls -a"
alias lS="ls -lSr"
alias ..="cd .."
alias install="apt-get install"
alias m='mutt'
alias s="scribble"

alias trunc="truncate -s0"

# Misc
alias hgrep="history | grep"
alias hex="hexdump -Cv"
alias n="nnn -d -x"

# git shortcuts
alias br="git checkout"
alias brl="git branch"
alias gg="git grep"
alias st="git status"
alias gd="git diff"
alias log='git log --color --graph --pretty=format:'\''%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'\'' --abbrev-commit'

if [ -x /usr/bin/fdfind ] ; then
  alias fd=fdfind
fi

alias dc="docker compose"
