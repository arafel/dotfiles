GO=$(which go)
if [ -n "$GO" ] ; then
  export GOPATH=$(go env GOPATH)
  addpath "$GOPATH/bin"
fi
