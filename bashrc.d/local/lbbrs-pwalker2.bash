export JTAG_IP=10.178.4.224

alias bounce_7252sb0="scp -r stbbri-sec-1:/local/home/pw921828/tmp/bounce/* ~/target/security/7252sb0"
alias bounce_7216b0="scp stbbri-sec-1:tmp/bounce/* /local/pwalker/target/security/7216b0"
alias bounce_74165a0="scp stbbri-sec-1:tmp/bounce/* /local/pwalker/target/security/74165a0"
alias bounce_nagra_ta="scp stbbri-sec-1:tmp/bounce/* /local/pwalker/target/security/nagra_ta"

export TERM=xterm-256color
export MAIL=$HOME/Mail/Broadcom/INBOX/
alias m2='mutt -F ~/.muttrc-fastmail'
alias ms="mutt -f ~/mx-search"
alias k="khal interactive"

addpath "$HOME/.local/bin"
alias bounce_72605a0='scp stbbri-sec-1:tmp/bounce/* /local/pwalker/target/security/72605a0'

export RUBY_CONFIGURE_OPTS="--disable-install-doc --with-openssl-dir=$(brew --prefix openssl@1.1)"
