addpath /opt/toolchains/stbgcc-6.3-1.7/bin/
addpath $HOME/.cache/rebar3/bin

export EDITOR=vim
export MAIL=$HOME/Mail/Broadcom/INBOX/
export TERM=xterm-256color

GPG_TTY=$(tty)
export GPG_TTY

export JTAG_IP=10.178.4.224

# alias jtagserial="telnet $JTAG_IP 2300"
# alias jtagserial2="telnet $JTAG_IP 2301"

# export WEBFLASHER_HOST=192.168.0.243
# alias flash="webflasher.pl -i $JTAG_IP -t -T http://$WEBFLASHER_HOST/webflasher/"
alias flash="webflasher.pl -i $JTAG_IP"
alias flashfirstimagez4="flash -s 0x100000"
alias flashfirstimage="flash -s 0x180000"
alias gp="globalprotect"
alias m2='mutt -F ~/.muttrc-imap'

# export ANDROID_HOME=$HOME/Android/Sdk
# addpath $ANDROID_HOME/emulator
# addpath $ANDROID_HOME/tools
# addpath $ANDROID_HOME/tools/bin
# addpath $ANDROID_HOME/platform-tools

alias k="khal interactive"
alias r="ranger"
