alias serv="cd ~/src/servicesmart"

addpath /Users/paul/Library/Python/3.7/bin
addpath ~/.npm-global/bin
addpath "$HOME/.serverless/bin"

# Set up NNTP variables
`netrc news.xcski.com NNTP`

export PGDATA="/Users/$USER/Library/Application Support/Postgres/var-10"

alias clearpgpid="rm $PGDATA/postmaster.pid"
alias nwjs="/Applications/nwjs.app/Contents/MacOS/nwjs"
