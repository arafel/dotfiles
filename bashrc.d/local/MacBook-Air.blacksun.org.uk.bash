# Set up NNTP variables
# echo "Running netrc"
# `netrc news.xcski.com NNTP`

# export PGDATA="/Users/$USER/Library/Application Support/Postgres/var-13"
alias clearpgpid="rm $PGDATA/postmaster.pid"
echo "Adding Flutter to path"
# addpath $HOME/opt/flutter-2.5.2/bin/
# addpath $HOME/Library/Python/3.9/bin

if [ "$(which pyenv)" != "" ] ; then
  eval "$(pyenv init -)"
fi

export BUPSTASH_KEY="/Users/paul/data/bupstash-my-drive.key"
export BUPSTASH_REPOSITORY="/Volumes/My drive/bupstash.repo/"
alias mutt-hd='mutt -F .muttrc-hd-gm'
