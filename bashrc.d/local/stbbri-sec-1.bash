if [ -x /usr/bin/dircolors ]; then
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

addpath $HOME/.local/bin

# some more ls aliases
alias ll='ls -l'
alias la="ls -a"
alias lS="ls -lSr"
alias lm="ls -ltr $HOME/mail/ | tail"
alias ..="cd .."

# git shortcuts
alias log2='git log --graph --oneline --all --decorate=full'

# Misc
alias v=vbindiff
alias ctags=ctags-exuberant

export JTAG_IP=10.178.4.115

# For use with jira-cli
export JIRA_AUTH_TYPE=bearer

# Misc work
alias jtagserial='ssh -o IdentityFile=~/.ssh/brcmstb_root root@$JTAG_IP -p 2200'
alias jtagserial2='ssh -o IdentityFile=~/.ssh/brcmstb_root root@$JTAG_IP -p 2201'

# Security
# WEBFLASH_SERVER=stbgit.broadcom.com/webflasher-test
alias flash='webflasher.pl -i $JTAG_IP'
alias flashfirstimagez4='flash -s 0x100000'
alias flashfirstimage='flash -s 0x180000'

alias goscratch='cd /local/scratch/pw921828'
alias goproj="cd /local/proj/irdeto"
alias gorelease="cd /local/releases/irdeto"
alias gomarvin='cd /local/scratch/pw921828/marvin && source .venv/bin/activate && source env.sh'
alias gozem='cd /local/scratch/pw921828/zem && source .venv/bin/activate'
alias golevel4='cd /local/scratch/pw921828/stblevel4 && source .venv/bin/activate'
alias gomview='cd /local/scratch/pw921828/marvinview && source .venv/bin/activate && source env.sh'

alias bounce='rsync -av $NEXUS_TOP/../$B_REFSW_OBJ_DIR/nexus/bin/* /projects/build/pw921828/$B_REFSW_BOOTLOADER_CHIP'
alias m="make && bounce"
alias debug='arm-linux-gdb -x gdb-7439.cmd'
alias put="ncftpput -f ~/.ncftp_login ."

alias cleanursr='if [ "$NEXUS_TOP" == "" ] ; then echo NEXUS_TOP not set ; else rm -r $NEXUS_TOP/../$B_REFSW_OBJ_DIR ; fi'

alias r="ranger"

PATH="$HOME/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;

GPG_TTY=$(tty)
export GPG_TTY
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"

export BORG_PASSCOMMAND='pass show borg_sec1_to_sec2'
