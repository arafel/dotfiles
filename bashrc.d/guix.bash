which guix 1>/dev/null
if [ $? -eq 0 ] ; then
  if [ -d "$HOME/.config/guix/" ] ; then
    export GUIX_PROFILE="$HOME/.config/guix/current"
    export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
    . "$GUIX_PROFILE/etc/profile"
  else
    echo "No current GUIX_PROFILE."
  fi
fi
if [ -n "$GUIX_ENVIRONMENT" ]; then
    if [[ $PS1 =~ (.*)"\\$" ]]; then
        PS1="${BASH_REMATCH[1]} [env]\\\$ "
    fi
fi
