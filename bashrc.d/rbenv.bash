if [ -f "$HOME/.rbenv/bin/rbenv" ] ; then
    addpath $HOME/.rbenv/bin
    eval "$($HOME/.rbenv/bin/rbenv init - bash)"
fi
