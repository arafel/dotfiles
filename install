#!/usr/bin/env bash
#
# Adapted from Junegunn Choi (junegunn.c@gmail.com)
# https://github.com/junegunn/dotfiles/blob/master/install

BASE=$(pwd)
IS_MAC=0
IS_LINUX=0
IS_FREEBSD=0

echo "Ensuring packages/system are up to date; please enter password for sudo if requested."
case $(uname -s) in
    Darwin) IS_MAC=1
            brew update
            ;;
    Linux) IS_LINUX=1
           sudo apt update
           ;;
    FreeBSD) IS_FREEBSD=1
             sudo pkg upgrade
             ;;
    *) echo "Unknown OS" ;;
esac

cd "$(dirname "${BASH_SOURCE[0]}")" || exit 1

MISSING=0
check_command() {
  CMD=$1
  FOUND=$(command -v $CMD)
  if [ "$FOUND" == "" ] ; then
    echo "You don't have $CMD."
    MISSING=1
  else
    echo "Found $FOUND."
  fi
}

COMMANDS="git realpath curl"
for cmd in $COMMANDS ; do
  check_command $cmd
done
if [ $MISSING -ne 0 ] ; then
  exit $MISSING
fi
unset COMMANDS MISSING

# RC files

mkdir -pv bak
for rc in *rc *profile tmux.conf bashrc.d mutt ; do
  if [ ! -r $rc ] ; then
    continue
  fi
  [ "$(realpath ~/."$rc")" == "$BASE/$rc" ] 
  if [ $? -ne 0 ] ; then
    [ -e ~/."$rc" ] && mv -v ~/."$rc" bak/."$rc"
    ln -sfv "$BASE/$rc" ~/."$rc"
  fi
done

mkdir -p ~/src/

# scripts
mkdir -p ~/bin
for bin in $BASE/bin/*; do
  BIN_NAME=$(basename $bin)
  [ $(realpath ~/bin/$BIN_NAME) == $bin ] 
  if [ $? -ne 0 ] ; then
    ln -svf "$bin" ~/bin
  fi
done

if [ $IS_MAC == 1 ] ; then
  # Homebrew
  [ -z "$(which brew)" ] &&
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

  brew install wget bash-completion macvim isync
fi

git config --global user.email "paul@blacksun.org.uk"
git config --global user.name "Paul Walker"
git config --global pull.rebase true
git config --global push.default current

if [ ! -d "$HOME/.rbenv" ] ; then
  git clone https://github.com/rbenv/rbenv.git "$HOME/.rbenv"
  git clone https://github.com/rbenv/ruby-build.git "$HOME/.rbenv/plugins/ruby-build"
else
  echo "Already have $HOME/.rbenv"
  git -C ~/.rbenv pull
  git -C ~/.rbenv/plugins/ruby-build pull
fi

if [ ! -d "$HOME/.asdf" ] ; then
  git clone https://github.com/asdf-vm/asdf.git "$HOME/.asdf"
  . bashrc.d/asdf.bash
else
  echo "Already have $HOME/.asdf"
  git -C ~/.asdf pull
fi

# Add and install these ones.
ADD_AND_INSTALL="golang ripgrep ag fd bat jq nodejs fzf github-cli task ninja meson"
if [ $IS_MAC == 1 ] ; then
  echo "Detected macOS, installing extra packages via asdf."
  ADD_AND_INSTALL="$ADD_AND_INSTALL git tmux vim"
fi

for plugin in $ADD_AND_INSTALL ; do
  asdf plugin add $plugin && \
    asdf install $plugin latest && \
    asdf global $plugin latest
done
unset ADD_AND_INSTALL

# Enable Yarn
corepack enable
asdf reshim
go install github.com/go-delve/delve/cmd/dlv@latest

if [ $IS_LINUX == 1 ] ; then
  sudo apt install fonts-firacode isync
  sudo apt build-dep mu4e
fi

if [ $IS_FREEBSD == 1 ] ; then
  sudo pkg install gmake vim rsync texinfo xsel-conrad
fi

# Just add these, for now.
ADD_ONLY="python tmux sbcl opam gohugo erlang elixir clojure"
for plugin in $ADD_ONLY ; do 
  asdf plugin add $plugin
done
unset ADD_ONLY

if [ $IS_MAC == 1 ] ; then
  echo 'To install SBCL:'
  echo 'CPATH=$(brew --prefix)/include:$CPATH LIBRARY_PATH=$(brew --prefix)/lib:$LIBRARY_PATH asdf install sbcl latest'
fi

if [ ! -d ~/.tmux/plugins/tpm ] ; then
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  if [ "$TMUX" != "" ] ; then
    # If we're currently in tmux, source the new file.
    tmux source-file ~/.tmux.conf
  fi
else
  echo "Already have ~/.tmux/plugins/tpm"
  git -C ~/.tmux/plugins/tpm pull
fi

# ./install-vim

if [ ! -d "$HOME/.emacs.d" ] ; then
  echo "Attempting to clone ~/.emacs.d, but you may not be logged in to GitHub."
  gh repo clone arafel/emacs.d ~/.emacs.d
else
  echo "Already have $HOME/.emacs.d"
fi
