# emacs mode
bindkey -e

bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line

# Even though 'showkey -a' gives the mappings above, these are required when using 
# MobaXterm at least (and possibly plain putty). NO idea why.
# See https://github.com/ohmyzsh/ohmyzsh/issues/4872 for speculation...
bindkey "\033[1~" beginning-of-line
bindkey "\033[4~" end-of-line

bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word
