addpath /usr/local/opt/node/bin
addpath /opt/toolchains/stbgcc-6.3-1.7/bin/

# addpath $HOME/bin/bento/wrappers/

export EJTAG_IP=192.168.0.80
export WEBFLASHER_HOST=192.168.0.243

alias jtagserial="telnet $EJTAG_IP 2300"
alias jtagserial2="telnet $EJTAG_IP 2301"
alias flash="webflasher.pl -i $EJTAG_IP -t -T http://$WEBFLASHER_HOST/webflasher/"
alias flashfirstimagez4="flash -s 0x100000"
alias flashfirstimage="flash -s 0x180000"
alias gp="globalprotect"
alias m3='mutt -F ~/.muttrc-imap'
alias m2='mutt -F ~/.muttrc-fastmail'
export MAIL=~/Mail/INBOX

# export ANDROID_HOME=$HOME/Android/Sdk
# addpath $ANDROID_HOME/emulator
# addpath $ANDROID_HOME/tools
# addpath $ANDROID_HOME/tools/bin
# addpath $ANDROID_HOME/platform-tools

# addpath $HOME/opt/jdk-15.0.2+7/bin

export TERM=xterm-256color
alias k="khal interactive"

`netrc news.xcski.com NNTP`
