[ -f ~/.config/tabtab/__tabtab.zsh ] && . ~/.config/tabtab/__tabtab.zsh || true
[ -f "$HOME/.cargo/env" ] && source "$HOME/.cargo/env" || true
[ -d "$HOME/.cargo/bin" ] && addpath "$HOME/.cargo/bin" || true
