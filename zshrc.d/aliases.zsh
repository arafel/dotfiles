if [ -x /usr/bin/dircolors ]; then
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi

# some more ls aliases
alias ll='ls -l'
alias la="ls -a"
alias lS="ls -lSr"
alias lm="ls -ltr $HOME/mail/ | tail"
alias ..="cd .."
alias install="apt-get install"
alias m='mutt'
alias s="slrn"
# alias irc="sc irc"
alias talker="tm talker"
alias db="dropbox.py"

alias trunc="truncate -s0"

# Misc
alias hgrep="history | grep"
alias hex="hexdump -Cv"

# git shortcuts
alias br="git checkout"
alias brl="git branch"
alias gg="git grep"
alias st="git status"
alias gd="git diff"

alias log='git log --color --graph --pretty=format:'\''%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'\'' --abbrev-commit'

if [ -x /usr/bin/fdfind ] ; then
  alias fd=fdfind
fi
