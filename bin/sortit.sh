#!/usr/bin/env bash
# MV=$(which smv)
if [ "$MV" == "" ] ; then
  MV="mv -i"
fi
echo "Using $MV to move files"

function getsourcefiles() {
  local src=$1
  unset i
  i=0
  unset files
  while IFS= read -r -u3 -d $'\0' file; do
    files[i++]="$file"
  done 3< <(ls "$src" | fzf -m --tac --print0)
  echo i $i
  if [ "$i" == "" ] ; then
    echo "You didn't choose any files."
    exit 1
  fi
}

function getdestdir() {
  dir=$(ls "$1" | fzf)
  if [ "$dir" == "" ] ; then
    echo "No destination selected."
    exit 1
  fi
}

if [ $# -lt 2 ] ; then
  echo $0 src_dir dest_parent
  exit 1
fi

src_dir=$1
dest_dir=$2

while [ 1 == 1 ] ; do
  getsourcefiles $src_dir
  getdestdir $dest_dir

  destpath=$(realpath "$2/$dir")

  for i in "${files[@]}"
  do
    # echo $MV "$1/${i}" "$destpath"
    $MV "$1/${i}" "$destpath"
  done
done
