# Fig pre block. Keep at the top of this file.
[[ -f "$HOME/.fig/shell/bashrc.pre.bash" ]] && builtin source "$HOME/.fig/shell/bashrc.pre.bash"
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

if [ "$(uname -s)" == "Darwin" ] ; then
  export CURRENT_OS=macos
else
  export CURRENT_OS=linux
fi

# Add an entry to PATH, iff it's not already there.
addpath() {
    wanted_path=$1
    at_end=$2
    FOUND=$(echo $PATH | grep $wanted_path)
    if [ $? -ne 0 ] ; then
        if [ "$at_end" == "" ] ; then
            export PATH=$wanted_path:$PATH
        else
            export PATH=$PATH:$wanted_path
        fi
    fi
}

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
# ... or force ignoredups and ignorespace
export HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# Don't escape variables when I hit tab
shopt -s direxpand

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#if [ -a "$TERM" == "xterm" ] ; then
#  # If it's available, use vscode-direct (which supports 16 million
#  # colours via the ';' sequences; required as PuTTY doesn't
#  # understand the ':' sequences).
#  #
#  # https://chadaustin.me/2024/01/truecolor-terminal-emacs/
#  if [[ -r /usr/share/terminfo/v/vscode-direct ]] ; then
#      export TERM=vscode-direct
#  else
#      export TERM=xterm-256color
#  fi
#else
export TERM=xterm-256color
#fi

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac
export PROMPT_DIRTRIM=3

addpath $HOME/bin
addpath $HOME/opt/bin end

export IPLAYER_OUTDIR="$HOME/Downloads/iplayer/"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors -a -f ~/.dircolors ]; then
    eval "`dircolors -b ~/.dircolors`"
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

alias gca="git commit --amend -a --no-edit"

if [ -d "$HOME/.local/bin" ] ; then
    addpath "$HOME/.local/bin"
fi

# Load any supplementary scripts
if [ -d "$HOME"/.bashrc.d ] ; then
    for config in "$HOME"/.bashrc.d/*.bash ; do
        source "$config"
    done
    unset -v config
fi
if [ -f ${HOME}/.bashrc.d/local/${HOSTNAME}.bash ] ; then
    echo "Loading ${HOME}/.bashrc.d/local/${HOSTNAME}.bash"
    source ${HOME}/.bashrc.d/local/${HOSTNAME}.bash
fi

# export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"
