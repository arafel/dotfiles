# Fig pre block. Keep at the top of this file.
[[ -f "$HOME/.fig/shell/zshrc.pre.zsh" ]] && builtin source "$HOME/.fig/shell/zshrc.pre.zsh"
# Add an entry to PATH, iff it's not already there.
addpath() {
    wanted_path=$1
    at_end=$2
    FOUND=$(echo $PATH | grep $wanted_path)
    if [ $? -ne 0 ] ; then
        if [ "$at_end" == "" ] ; then
            export PATH=$wanted_path:$PATH
        else
            export PATH=$PATH:$wanted_path
        fi
    fi
}

autoload -Uz compinit && compinit

addpath $HOME/bin

# Load any supplementary scripts
if [ -d "$HOME"/.zshrc.d ] ; then
    for config in "$HOME"/.zshrc.d/*.zsh ; do
        source "$config"
    done
    unset -v config
fi
if [ -f ${HOME}/.zsh.d/local/${HOSTNAME}.zsh ] ; then
    source ${HOME}/.zsh.d/local/${HOSTNAME}.zsh
fi

# Fig post block. Keep at the bottom of this file.
[[ -f "$HOME/.fig/shell/zshrc.post.zsh" ]] && builtin source "$HOME/.fig/shell/zshrc.post.zsh"
