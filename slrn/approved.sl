% This variable will hold the followup_custom_headers as defined in the
% .slrnrc file.
variable Default_Followup_Headers;

Default_Followup_Headers = get_variable_value ("followup_custom_headers");
define followup_hook ()
{
   variable h;

   h = Default_Followup_Headers;
   
   if ((0 == strncmp (current_newsgroup, "alt.sysadmin.recovery", 21)) |
       (0 == strncmp (current_newsgroup, "alt.tech-support.recovery", 25)) |
       (0 == strncmp (current_newsgroup, "bofh.", 5))
      )
     {
	% Generate Approved header
	h = sprintf ("%sApproved: probably not\n", h);
     }

   if ((0 != strncmp(current_newsgroup, "comp.", 5)) &
       (0 != strncmp(current_newsgroup, "rec.", 4)) &
       (0 != strncmp(current_newsgroup, "bofh.", 5))
      )
   {
       h = sprintf("%sX-No-Archive: Yes\n", h);
   }

   set_string_variable ("followup_custom_headers", h);
}
