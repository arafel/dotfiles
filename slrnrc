% -*- slang -*-
% This is a sample startup file for the slrn news reader.  The percent
% character is used for comments.

% Note OS/2 and Win32 users: 
%   To separate directories you can either use a single '/' or
%   double '\\'. Single backslashes are not supported.  For example, use
%   "C:\\home\\file.txt" or "C:/home/file.txt" but NOT "C:\home\file.txt"

% SERVER to NEWSRC mapping
%server "hsdndev.harvard.edu" ".jnewrc-hsdndev"
%server "news.uni-stuttgart.de" ".jnewsrc-stuttgart"

% The next line is for those servers that require a password.
%nnrpaccess "HOSTNAME" "USERNAME" "PASSWORD"
%nnrpaccess "HOSTNAME" "USERNAME" ""         % Prompt for password
%nnrpaccess "HOSTNAME" ""         ""         % Prompt for username and password

%nnrpaccess news.xcski.com blacksun2 vD82QRK6

set hostname "blacksun.org.uk"
set username "paul"

set realname "Paul Walker"
set replyto  "paul@blacksun.org.uk"

% Name of signature file to use
set signature ".signature"

% The quote string will be used when following up/replying
set quote_string ">"

% If non-zero, signature will not be included in quoted text of followups
set followup_strip_signature 0

% If set to 1, slrn will not allow you to post articles that contain 
% non-quoted lines whose length exceeds 80 characters.  If set to 2, a warning
% will be generated.
set reject_long_lines 1

% This regular expression defines lines that are consider to be quoted lines.
% It says that any lines beginning with 0-2 spaces followed by a either
% a >, <, :, |, or = character is a quoted line.
ignore_quotes "^ ? ?[><:=|]"

% This sets the follow-up and reply strings.  Here, the following format 
% specifiers are recognized: 
%    %d:date, %r:real name, %f:email address, %s:subject,
%    %m:msgid, %n:newsgroups, %%: percent 
set followup_string "On %d, %r wrote:"
set reply_string "In %n, you wrote:"

%Custom headers to add to NEW posts
%set custom_headers "Approved: probably not\nX-No-Archive: yes\n"
set custom_headers "X-No-Archive: yes\n"

% Custom headers for followup/reply.  These can use format specifiers as
% in the 'followup' string variable.
%set followup_custom_headers "Approved: probably not\nX-No-Archive: yes\n"
%set followup_custom_headers "X-No-Archive: yes\n"
%set reply_custom_headers "X-Comment-to: (%r)\n"

% Headers to show when viewing an articles.  This is a comma-separated
% list of strings that specify what headers to show.  Note that these 
% strings are not regular expressions.  However, one may use, e.g.,
% "X-" to match any header beginning with "X-".  Similarly, "F" will 
% match "From:" and "Followup".
visible_headers "From:,Subject:,Newsgroups:,Followup-To:,Reply-To:,Date:,Approved:"

% WWW browser to use.  The 'U' key in article mode searches the current article
% for a URL and then calls X browser if slrn appears to be run in X windows
% and non_Xbrowser otherwise.  Note: The second Xbrowser form assumes that
% netscape is already running.
set non_Xbrowser "lynx '%s'"
set Xbrowser "netscape '%s' &"
%set Xbrowser            "netscape -remote 'openURL(%s)' &"

% If autobaud is present, output rate will be synced to baud rate
%autobaud

% if non-zero, display first article when entering article-mode.
set show_article 0

% if non-zero, show description of newsgroup if available
%set show_descriptions 1

% column where group descriptions start
%set group_dsc_start_column 40

% If non-zero, backups of the newsrc file are not performed.
set no_backups 0

% If 0, do not beep terminal.  If 1, send audible beep.  If 2, send only 
% visible bell.  If 3, send both.
set beep 2

% If non-zero, slrn will automatically un-subscribe new groups.  
% The default is 0.
set unsubscribe_new_groups 0

% If non-zero, all subject titles will be displayed even though many are
% duplicated.  A zero value makes the screen look less busy.
set show_thread_subject 0

% If non-zero, slrn will draw the thread tree using simple ascii characters
set simulate_graphic_chars 0

% Enable xterm/win32 mouse support: 1 to enable, 0 to disable
set mouse 0

% Draw cursor bar ala tin instead of slrn "->" cursor?
set display_cursor_bar	0

% Set to 1 to have slrn scroll by full page instead of by line.
set scroll_by_page 0

% Setting this to 1 will cause an article to automatically be marked as read
% when viewed.  Setting this to zero means that it is up to the user to 
% explicitly mark an article as read.
set auto_mark_article_as_read 1

% Setting either of these to 0 will enable you to move directly to the
% next article/group without confirmation
set query_next_group 1
set query_next_article 1

% if zero, you will not see the "next group:" prompt.  This is NOT the same
% as the 'query_next_group' variable.
%set prompt_next_group 1

% Set this to 0 for no-confirmation on follow, reply, quit, etc...
set confirm_actions 1

% If 0, do not display the name of the author.
% If 1, display subject then name.  If 2, display name then subject.
%set author_display 2

% if 1, display author realname, if 0, display full address
%set display_author_realname 1

% If 0, save all groups when writing newsrc file
% if 1, do not save any unsubscribed groups
% if 2, do not save any unread-unsubscribed groups
set write_newsrc_flags 0

% If non-zero, you will be prompted whether or not a Cc header will be
% generated on the followup.
set cc_followup 0
% set cc_followup_string "[This message has also been posted.]"

% Set to 0 to turn off display of ~ at end of article
set use_tilde 1

% Filename where articles you have posted are archived.
set save_posts "News/Sent"
set save_replies "News/Sent_Emails"

%File where failed posts are appended.  Use "" to disable saving.
%set failed_posts_file "dead.letter"

% The sendmail command allows you to substitute another mailer.  Be sure that 
% it implements the same interface as sendmail!
%set sendmail_command "/usr/lib/sendmail -oi -t -oem -odb"
%set sendmail_command "/usr/exim/bin/exim -oi -t -oem -odb"
set sendmail_command "~/bin/mymuttmsmtp -a Fastmail"

% Name of score file  (relative to HOME directory)
set scorefile "News/Score"

% Articles scoring min_high_score and above constitute a high scoring article.
% Articles scoring below max_low_score define a low scoring article.
% Articles scoring at or below kill_score will be killed.
set min_high_score 1
set max_low_score 0
set kill_score -9999

% Name of directory where decoded files are placed (relative to HOME)
set decode_directory "downloads"
% Directory where all other files are saved.
set save_directory "News"
% Directory where postponed articles are placed.  (Make sure it exists)
set postpone_directory "News/postponed"

% If non-zero, abort posting or email operation if the file was not modified
% by the editor.
set abort_unmodified_edits 1

% Command used to invoke editor.  In the following example, %s represents
% the file name and %d represents the starting line number
%set editor_command "jed '%s' -g %d -tmp"
set editor_command "joe '%s'"
%set editor_command "~/.edit.sh '%s'"

% Editor command for editing posts, mail, and score files.  Note the use of 
% single quotes around %s.  This is only necessary if you use filenames 
% with spaces in them.
%set mail_editor_command "jed '%s' -g %d -tmp"
%set post_editor_command "jed '%s' -g %d -tmp"
%set score_editor_command "jed '%s' -g %d -tmp"

% If  non-zero, files used for posting, followup and reply will be regarded as
% temporary files in the directory specified by the TMPDIR environment 
% variable or /tmp.
set use_tmpdir 0

% If 0, do not sort.  If 1, perform threading.  If 2, sort by subject
% If 3, thread then sort result by subject
% If 4, sort by score.  If 5, thread then sort by score.
% If 6, sort by score and subject
% If 7, thread, then sort by score and subject
% If 8, sort by date with most recent first
% If 9, thread, then sort by date with most recent first
% If 10, sort by date with most recent last
% If 11, thread then sort by date with most recent last
set sorting_method 7
%set display_score 1

% If 1, a header with a new subject will start a new thread.
set new_subject_breaks_threads 0

% If non-zero, threads will be uncollapsed when a group is entered
set uncollapse_threads 1

% If non-zero, slrn will read the active file when starting.   This may lead to
% faster startup times IF your network connection is fast.  If it slow, 
% DO NOT USE IT.  If you can, I recommend setting it to 1.
set read_active 1

% If non-zero and read_active is zero, slrn will attempt to use the NNTP
% XGTITLE command when listing unsubscribed groups.
%set use_xgtitle 0

% What to wrap when wrapping an article:
%  0 or 4 ==> wrap body
%  1 or 5 ==> wrap headers, body
%  2 or 6 ==> wrap quoted text, body
%  3 or 7 ==> wrap headers, quoted text, body
% The higher number indicates that every article will be automatically 
% wrapped.
set wrap_flags 6

% Maximum number of articles to read before slrn will prompt.  Default is 100.
% Set this to zero to turn of prompting.
set query_read_group_cutoff 100

% Numes of lines to read from the server between percentage counter updates
% This number will vary with the speed of the connection to your server
set lines_per_update 100

% Valid character sets: isolatin, ibm850, next, koi8
% ibm850 is the default on OS/2
% next is the default on NeXT machines
% isolatin is the defaule on all other systems.
%set charset isolatin

%---------------------------------------------------------------------------
% Mime support
%---------------------------------------------------------------------------
% set use_mime 1
%set mime_charset "iso-8859-1"
%set mime_charset "utf-8"
% If non-zero, call metamail for mime formats that slrn does not handle
set use_metamail 0
set metamail_command "metamail"

% If non-zero, header numbers will be displayed in the left-margin of the
% header window.  These numbers may be used as ``thread selectors''.
set use_header_numbers 1

% If non-zero, prompt for reconnection if the NNTP connection drops.  If zero,
% attempt reconnection without asking user.
%set query_reconnect 1

% Character to use to hide spoiler text:
set spoiler_char '*'

% set to 0 to keep the display still, and just reveal the spoiler
% set to 1 to start a new page when spoiler is revealed
% set to 2 to keep the display still, and reveal ALL spoilers
% set to 3 to start new page and reveal ALL spoilers
set spoiler_display_mode 2
 
% Help text to be displayed at bottom of screen in various modes:
%set art_help_line "bla bla"
%set header_help_line "more bla bla"
%set group_help_line "and even more"


%---------------------------------------------------------------------------
%  Local spool configuration
%---------------------------------------------------------------------------
% set spool_inn_root "/var/lib/news"
% set spool_root "/var/spool/news"
% set spool_nov_root "/var/spool/news/over.view"
%
%% -- The following filenames are relative to spool_inn_root unless they
%%    start with "/".
% set spool_active_file "data/active"
% set spool_activetimes_file "data/active.times"
% set spool_newsgroups_file "data/newsgroups"

%% -- The following filenames are relative to spool_nov_root
% set spool_nov_file ".overview"

%If set to 1, then spool.c will actually check each article file exists when
%reading an overview file.  This adds a perceptible delay (especially in a
%large spool directory), so I've made it an option.  With some servers it is
%almost redundant, whereas with others which don't expire entries from
%overview files regularly, it's almost vital.  If the users sees a lot of
%"article not available" errors, they should probably try turning it on.
% set spool_check_up_on_nov 0

%---------------------------------------------------------------------------
%  GroupLens Support
%---------------------------------------------------------------------------
%set use_grouplens 1
%color grouplens_display blue white
%set grouplens_host		"grouplens.cs.umn.edu"
%set grouplens_port		9000
%set grouplens_pseudoname	"YOUR_PSEUDONAME"
%grouplens_add "rec.cooking.recipes"
%grouplens_add "comp.os.linux.misc"


%---------------------------------------------------------------------------
% Colors
%---------------------------------------------------------------------------
color header_number	"lightblue"	"black"
color header_name	"lightblue"	"black"
color normal		"lightgray"	"black"
color error		"red"		"lightgray"
color status		"yellow"	"blue"
color group		"black"		"lightgray"
color article		"lightgray"	"black"
color cursor		"brightgreen"	"lightgray"
color author		"magenta"	"black"
color subject		"brightblue"	"black"
color headers		"brightblue"	"black"
color menu		"yellow"	"blue"
color menu_press	"blue"		"yellow"
color tree		"red"		"black"
color quotes		"yellow"	"black"
color thread_number	"blue"		"lightgray"
color high_score	"red"		"lightgray"
color signature		"red"		"black"
color description	"blue"		"lightgray"
color tilde		"green"		"black"
color response_char	"green"		"lightgray"
%-----------------------------------------------------
% Monochrome attributes for monochrom terminals.   One or more attributes
% may be specified.
%-----------------------------------------------------
mono normal		"none"
mono header_number	"none"
mono header_name	"bold"
mono error		"blink" "bold"
mono status		"reverse"
mono group		"bold"
mono article		"none"
mono cursor		"bold"	"reverse"
mono author		"none"
mono subject		"none"
mono headers		"bold"
mono menu		"reverse"
mono menu_press		"none"
mono tree		"bold"
mono quotes		"underline"
mono thread_number	"bold"
mono high_score		"bold"
mono signature		"none"
mono description	"none"
mono response_char	"bold"

%------------------------  Group keymap------------------------------------

setkey group	post_postponed	"\eP"
setkey group	add_group	"A"    %  add a new newsgroup
setkey group	bob		"\e<"  %  beg of buffer
setkey group	bob		"^K\eOA"
setkey group	bob		"^K\e[A"
setkey group	catchup		"C"    %  mark group as read
setkey group	line_down	"\eOB" %  next group
setkey group	line_down	"\e[B"
setkey group	line_down	"^N"
setkey group	eob		"\e>"  %  end of buffer
setkey group	eob		"^K\eOB"
setkey group	eob		"^K\e[B"
setkey group	group_search_forward	"/"
setkey group	help		"?"
setkey group	page_down	"^D"   %  next page of groups
setkey group	page_down	"\e[6~"
setkey group	page_down	"^V"
setkey group	page_up		"\eV"  %  previous page of groups
setkey group	page_up		"^U"
setkey group	page_up		"\e[5~"
setkey group	post		"P"
setkey group	quit		"Q"
setkey group	redraw		"^L"
setkey group	redraw		"^R"
setkey group	refresh_groups	"G"
setkey group	save_newsrc	"X"
setkey group	select_group	"\r"   %  read articles from group
setkey group	select_group	" "
setkey group	subscribe	"S"    %  subscribe to group (See unsubscribe)
setkey group	suspend		"^Z"
setkey group	toggle_group_formats	"\033A"
setkey group	toggle_scoring	"K"
setkey group	toggle_hidden	"l"
setkey group	toggle_list_all	"L"
setkey group	unsubscribe	"U"    %  unsubscribe
setkey group	line_up		"\eOA" %  previous line
setkey group	line_up		"\e[A"
setkey group	line_up		"^P"

%---------------- Article mode keymap -------------------------------------

setkey article	post_postponed	"\eP"
setkey article	goto_article	"j"
setkey article	pipe		"|"
setkey article	skip_quotes	"\t"

setkey article	header_page_up		"^U"
setkey article	header_page_up		"\e[5~"
setkey article	header_page_up		"\eV"

setkey article	header_page_down	"\e[6~"
setkey article	header_page_down	"^D"
setkey article	header_page_down	"^V"

setkey article post "P"
setkey article toggle_header_formats "\ea"
setkey article get_parent_header "\ep"
setkey article	catchup_all	"c"
setkey article	catchup_all	"\ec"
setkey article	uncatchup_all	"\eu"
setkey article	catchup		"\eC"
setkey article	uncatchup	"\eU"
setkey article	article_page_down	" "	% scroll to next page of article or select article
setkey article	article_page_up	"^?"	% scroll to next page of article
setkey article	article_page_up	"b"	% (scroll_up or article_pageup)
setkey article  article_line_up	"\e\e[A"	% Scroll article one line up
setkey article  article_line_up	"\e\eOA"
setkey article  article_line_down	"\e\e[B"	% Scroll article one line down
setkey article  article_line_down	"\e\eOB"
setkey article	article_line_down  "\r"
setkey article  article_search	"/"	% Search forward through article
setkey article  author_search_forward	"a"	% Search forward for an author
setkey article  author_search_backward	"A"	% Search backward for an author
setkey article  cancel		"\e^C"	% Cancel the article
setkey article  delete		"d"	% Mark current article as read and move to the next unread one
setkey article  header_line_down	"^N"	% Move to the next article
setkey article  header_line_down	"\e[B"
setkey article  header_line_down	"\eOB"
setkey article  mark_spot	";"	% Set mark at current article
setkey article  exchange_mark	","	% Set the mark and return to the location of the previous mark.
setkey article  followup	"f"	% Followup on the article
setkey article  forward		"F"	% Forward the article to someone
setkey article  help		"?"	% Show help screen
setkey article  hide_article	"H"	% Hide the article window.
setkey article	zoom_article_window	"z"   % Zoom/UnZoom article window
setkey article  article__eob	">"	% goto the end of the article
setkey article  article_left	"\eOD"
setkey article  article_left	"\e[D"
setkey article  next		"n"	% next unread article
setkey article  skip_to_next_group	"N"	% next group
setkey article  previous	"p"	% previous unread article
setkey article  quit		"q"	% Quit back to group mode.
setkey article  redraw		"^L"	% Redraw the display
setkey article  redraw		"^R"
setkey article  reply		"r"	% Reply to the author of the current article
setkey article  article_bob	"<"	% goto the beginning of the article
setkey article  article_right	"\e[C"
setkey article  article_right	"\eOC"
setkey article  save		"O"	% append to a file in Unix mail format
setkey article  subject_search_forward	"s"	% Search forward/backward for an article with a specific subject
setkey article  subject_search_backward	"S"
setkey article  suspend		"^Z"	% Suspend the newsreader
setkey article  toggle_collapse_threads "\et"
setkey article	toggle_rot13	"\eR"
setkey article	toggle_sort	"\eS"
setkey article  toggle_headers	"t"	% Toggle the display of some headers on and off
setkey article  toggle_quotes	"T"
setkey article  undelete    	"u"	% Mark the current article as unread
setkey article  header_line_up	"^P"	% Move to the previous article
setkey article  header_line_up	"\eOA"
setkey article  header_line_up	"\e[A"
setkey article	header_bob	"\e<"	% Move to first article in list
setkey article	header_eob	"\e>"	% Move to last article in list
setkey article	shrink_article_window	"^^"	% Ctrl-6 or Ctrl-^	% Shrink header window
setkey article	enlarge_article_window	"^"	% Shift-6 or just ^	% Enlarge header window
setkey article  next_high_score	"!"

setkey article  print		"y"

setkey article  wrap_article	"W"    %  toggle wrapping

%setkey article  skip_to_prev_group ""     % No default binding
%setkey article  fast_quit          ""     % No default binding

% This is a special hack for HP terminals to get the arrow keys working.  
% Are there any other terminals being used with non-ANSI arrow keys?
#if$TERM hpterm
  setkey group up "\eA"
  setkey group down "\eB"
  setkey article down "\eB"
  setkey article up "\eA"
  setkey article left "\eD"
  setkey article right "\eC"
#endif

% Command prompt keymap
setkey readline bol		 "^A"		% Beginning of line
setkey readline eol		 "^E"		% End of line
setkey readline right		 "\e[C"		% Move right
setkey readline left		 "\e[D"		% Move left
setkey readline bdel		 "^H"		% Delete backward
setkey readline bdel		 "^?"		% Delete Backward
setkey readline del		 "^D"		% Delete Foreword
setkey readline deleol		 "^K"		% Delete to End of line
setkey readline trim		 "\e/"		% Trim whitespace
setkey readline quoted_insert	 "^Q"		% Quoted insert

%interpret /usr/local/share/slrn/search.sl
% Path is relative to $HOME
interpret .slrn/approved.sl
